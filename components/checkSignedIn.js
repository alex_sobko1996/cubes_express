function checkSignInOnHome (req, res, next) {
	if (!req.session.isSignedIn) {
		res.status(401);
		res.redirect('/signIn');
	} else {
		return next();
	}
}

function checkSignInUp (req, res, next) {
	if (!req.session.isSignedIn) {
		return next();
	} else {
		res.redirect('/');
	}
}

export {checkSignInOnHome, checkSignInUp};