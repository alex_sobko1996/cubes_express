import express from 'express';
import ResultsController from '../controllers/resultsController.js';

const router = express.Router();

/* GET results. */
router.get('/', new ResultsController().getResults);
router.post('/', new ResultsController().save);

export default router;