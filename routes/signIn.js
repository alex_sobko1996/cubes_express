import express from 'express';
import SignInController from '../controllers/signInController.js';
import { checkSignInUp } from '../components/checkSignedIn.js';

const router = express.Router();

/* GET index page. */
router.get('/', checkSignInUp,  new SignInController().index);
router.post('/', new SignInController().signIn);

export default router;