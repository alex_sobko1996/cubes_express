import express from 'express';
import SignOutController from '../controllers/signOutController.js';

const router = express.Router();

/* GET index page. */
router.get('/', new SignOutController().signOut);

export default router;