import express from 'express';
import IndexController from '../controllers/indexController.js';
import { checkSignInOnHome } from '../components/checkSignedIn.js';

const router = express.Router();

/* GET index page. */
router.get('/', checkSignInOnHome, new IndexController().index);

export default router;