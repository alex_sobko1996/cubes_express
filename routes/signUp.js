import express from 'express';
import SignUpController from '../controllers/signUpController.js';
import { checkSignInUp } from '../components/checkSignedIn.js';

const router = express.Router();

/* GET index page. */
router.get('/', checkSignInUp, new SignUpController().index);
router.post('/', new SignUpController().signUp);

export default router;