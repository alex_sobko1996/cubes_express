import fs from 'fs';

const PATH = './data/results.json';

export default class Results {
	static getTop10data(done) {
  		fs.readFile(PATH, (err, data) => {
    		if (err) {
      			return done(err);
    		}
    		data = JSON.parse(data).slice(0, 10);
    		done(null, JSON.stringify(data));
  		});
	}

	getResults(req, res, next) {
		Results.getTop10data((err, data) => {
    		if (err) {
      			next(err);
    		} else {
      			res.json(data).end();
    		}
  	});
	}

  save (req, res, send) {
    fs.readFile(PATH, (err, data) => {
      if (err) {
        return next(err);
      }
      data = JSON.parse(data);
      data.push({
        name: req.session.login,
        points: req.body.points
      });
      fs.writeFile(PATH, JSON.stringify(data.sort((a, b) => b.points - a.points)), err => {
        if (err) {
          return next(err);
        }
      });
    });
  }
}