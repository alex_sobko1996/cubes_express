import fs from 'fs';

const PATH = './data/users.json';

export default class SignUpController {
	index (req, res, next) {
		res.render('pages/signUp', {
			title: 'Sign Up'
		});
	}

	signUp (req, res, next) {
		fs.readFile(PATH, (err, data) => {
			if (err) {
				return next(err);
			}

			let newUser = req.body, 
				userAlreadyExists = false;
			data = JSON.parse(data);

			data.forEach(item => {
				if (item.login === newUser.login) {
					userAlreadyExists = true;
				}
			});

			if (newUser.password !== newUser.confirmedPassword) {
				res.render('pages/signUp', {
					title: 'Sign Up',
					name: newUser.name,
					login: newUser.login,
					message: 'Passwords are different!'
				});
			}
			else if (newUser.name === '') {
				res.render('pages/signUp', {
					title: 'Sign Up',
					name: newUser.name,
					login: newUser.login,
					message: 'Name is required to continue!'
				});
			}
			else if (userAlreadyExists) {
				res.render('pages/signUp', {
					title: 'Sign Up',
					name: newUser.name,
					login: newUser.login,
					message: 'User already exists!'
				});
			} else {
				data.push({
					name: newUser.name,
					login: newUser.login,
					password: newUser.password
				});	

				fs.writeFile(PATH, JSON.stringify(data), err => {
					if (err) {
						return next(err);
					}

					req.session.isSignedIn = true;
					req.session.login = req.body.login;	
					res.redirect('/');
				});
			}
		});
	}
}