import fs from 'fs';

const PATH = './data/users.json';

export default class SignInController {
	index (req, res, next) {
		res.render('pages/signIn', {
			title: 'Sign In'
		});
	}

	signIn (req, res, next) {
		fs.readFile(PATH, (err, data) => {
			if (err) {
				return next(err);
			}

			let userFounded = false, incorrectPassword = false;
			data = JSON.parse(data);

			data.forEach(item => {
				if (item.login === req.body.login && item.password === req.body.password) {
					req.session.isSignedIn = true;
					req.session.login = req.body.login;
					userFounded = true;
				}
				if (item.login === req.body.login) {
					incorrectPassword = true;
				}
			});

			if (userFounded) {
				res.redirect(301, '/');
			} else {
				let message = '';
				if (incorrectPassword) {
					message = 'Your password is incorrect!';
				} else {
					message = 'Oops! Looks like you don\'t have an account!';
				}
				res.render('pages/signIn', {
					title: 'Sign In',
					login: req.body.login,
					password: req.body.password,
					message: message
				});
			}
		});
	}
}