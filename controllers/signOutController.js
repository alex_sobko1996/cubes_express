export default class SignOutController {
	signOut (req, res, next) {
		req.session.destroy(err => {
			if (err) {
				return next(err);
			}
			res.redirect(301, '/signIn');
		});
	}
}