export default class IndexController {
	index(req, res, next) {
		res.setHeader("Content-Type", "text/html");
		res.render('index', {
			title: 'Home',
			login: req.session.login
		});
	}
}